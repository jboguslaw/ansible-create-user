Role Name
=========

Create/delete/update users and password base on tags. This role base on ansible 'user' module, but version used by this role is changed to allow provide user uid range. 

Windows is supported via WinRM

Requirements
------------



Role Variables
--------------

List of users to add should be create in different variable files. Example of files can be found in vars/main.yml.  Inside the file must be defined project name (base on this name subdirectory under credential/ directowy will be created and all users password will be stored there)

tags:
 - lin_delsshkeys - remove all ssh keys from user account
 - lin_addsshkeys - add new ssh keys to user account
 - lin_pwexpired - set password expiration for user
 - lin_removegroups - remove groups
 - lin_removeusers - remove users
 - lin_newusers - for create new user
 - removeusers - remove users from list (including home directory!)
 - pwexpired - set password expiration (user must change password during first login)
 - debugs - display debugs information
 - win_newuser - for create new user
 - win_removeusers - remove users from list (including home directory!)
 - move_home - move existing user home into new location (new_home var)



Dependencies
------------



Example Playbook
----------------

- hosts: servers
  roles:
    - { role: ansible-create-user, users_list: example_users_for_project.yml, tags: ['newusers','pwexpired'] }

License
-------

BSD

Author Information
------------------

Jakub K. Boguslaw <jboguslaw@gmail.com>. Please contact me if you have any issue /problem
